class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.belongs_to :user
      t.belongs_to :bias
      t.references :voteable, polymorphic: true, index: true
      t.string :option
      t.timestamps null: false
    end
  end
end
