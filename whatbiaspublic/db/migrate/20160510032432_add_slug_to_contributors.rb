class AddSlugToContributors < ActiveRecord::Migration
  def change
    add_column :contributors, :slug, :string
    add_index :contributors, :slug
  end
end
