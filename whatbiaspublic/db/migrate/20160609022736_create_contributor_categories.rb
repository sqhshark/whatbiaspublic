class CreateContributorCategories < ActiveRecord::Migration
  def change
    create_table :contributor_categories do |t|
      t.belongs_to :category, index: true
      t.belongs_to :contributor, index: true
      t.timestamps null: false
    end

    drop_table :contributorcategories
  end
end
