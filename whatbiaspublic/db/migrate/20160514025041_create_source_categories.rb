class CreateSourceCategories < ActiveRecord::Migration
  def change
    create_table :source_categories do |t|
      t.belongs_to :category, index: true
      t.belongs_to :source, index: true
      t.timestamps null: false
    end

    drop_table :sourcecategories
  end
end
