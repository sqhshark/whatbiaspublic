class AddApprovedToContributors < ActiveRecord::Migration
  def change
    add_column :contributors, :approved, :boolean, default: false
  end
end
