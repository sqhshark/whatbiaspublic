class SourceThroughCategories < ActiveRecord::Migration
  def change
    create_table :sourcecategories do |t|
      t.belongs_to :category, index: true
      t.belongs_to :source, index: true
      t.timestamps null: false
    end

    create_table :contributorcategories do |t|
      t.belongs_to :category, index: true
      t.belongs_to :contributor, index: true
      t.timestamps null: false
    end

    drop_table :sourcebiases
    drop_table :contributorbiases
  end
end
