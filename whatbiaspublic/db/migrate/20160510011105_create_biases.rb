class CreateBiases < ActiveRecord::Migration
  def change

    create_table :contributors do |t|
      t.belongs_to :source
      t.string  :name
      t.string  :picture
      t.timestamps null: false
    end
    
    create_table :categories do |t|
      t.string  :name
      t.timestamps null: false
    end

    create_table :biases do |t|
      t.belongs_to :category
      t.string  :name
      t.string  :picture
      t.string  :option1
      t.string  :option3
      t.timestamps null: false
    end
    
    create_table :sourcebiases do |t|
      t.belongs_to :bias, index: true
      t.belongs_to :source, index: true
      t.timestamps null: false
    end
    
    create_table :contributorbiases do |t|
      t.belongs_to :bias, index: true
      t.belongs_to :contributor, index: true
      t.timestamps null: false
    end
    
  end
end
