class ChangeGender < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.change :affiliation, :string
    end
  end
end
