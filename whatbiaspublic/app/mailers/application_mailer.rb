class ApplicationMailer < ActionMailer::Base
  default from: "noreply@whatbias.com"
  layout 'mailer'
end