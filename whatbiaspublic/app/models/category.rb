class Category < ActiveRecord::Base
  has_many :source_categories, dependent: :destroy
  accepts_nested_attributes_for :source_categories
  has_many :sources, through: :source_categories, dependent: :destroy
  has_many :biases, dependent: :destroy

  has_many :contributor_categories, dependent: :destroy
  accepts_nested_attributes_for :contributor_categories
  has_many :contributors, through: :contributor_categories, dependent: :destroy

  extend FriendlyId
  friendly_id :name

  validates :name,  presence: true, uniqueness: { case_sensitive: false }
end
