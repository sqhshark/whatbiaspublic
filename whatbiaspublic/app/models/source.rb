class Source < ActiveRecord::Base
  has_many :contributors, dependent: :destroy
  has_many :source_categories, dependent: :destroy
  accepts_nested_attributes_for :source_categories
  has_many :categories, through: :source_categories, dependent: :destroy
  has_many :votes, as: :voteable, dependent: :destroy

  extend FriendlyId
  friendly_id :name, use: [:slugged, :history]
  def should_generate_new_friendly_id?
    name_changed?
  end

  validates :name,  presence: true, uniqueness: { case_sensitive: false , message: "name not unique"}

  mount_uploader :logo, AttachmentUploader
  validates :logo, :presence => true
end
