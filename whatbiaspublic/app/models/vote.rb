class Vote < ActiveRecord::Base
  belongs_to :user
  belongs_to :voteable, polymorphic: true
  belongs_to :bias

  validates :option,  presence: true
end
