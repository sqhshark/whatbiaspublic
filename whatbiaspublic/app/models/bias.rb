class Bias < ActiveRecord::Base
  has_one :category
  has_many :votes, dependent: :destroy

  validates :name,  presence: true, uniqueness: { case_sensitive: false }
  validates :option1,  presence: true
  validates :option3,  presence: true
  mount_uploader :picture, AttachmentUploader
  validates :picture, :presence => true


  def all_votes(voteable)
    voteable.votes.where(bias_id: self.id).count
  end

  def option1_votes(voteable)
    voteable.votes.where(option: 1, bias_id: self.id).count
  end

  def option1_percent(voteable)
    if all_votes(voteable) != 0
      (((option1_votes(voteable).to_f/all_votes(voteable).to_f))*100)
    else
      0
    end
  end

  def option1_percent_display(voteable)
    option1_percent(voteable).floor
  end

  def option2_votes(voteable)
    voteable.votes.where(option: 2, bias_id: self.id).count
  end

  def option2_percent(voteable)
    if all_votes(voteable) != 0
      (((option2_votes(voteable).to_f/all_votes(voteable).to_f))*100)
    else
      0
    end
  end

  def option2_percent_display(voteable)
    option2_percent(voteable).floor
  end

  def option3_votes(voteable)
    voteable.votes.where(option: 3, bias_id: self.id).count
  end

  def option3_percent(voteable)
    if all_votes(voteable) != 0
      (((option3_votes(voteable).to_f/all_votes(voteable).to_f))*100)
    else
      0
    end
  end

  def option3_percent_display(voteable)
    option3_percent(voteable).floor
  end

  def option2
    "Unbiased"
  end

end
