class Contributor < ActiveRecord::Base
  belongs_to :source
  has_many :contributor_categories, dependent: :destroy
  accepts_nested_attributes_for :contributor_categories
  has_many :categories, through: :contributor_categories, dependent: :destroy
  has_many :votes, as: :voteable, dependent: :destroy

  extend FriendlyId
  friendly_id :name, use: [:slugged, :history]
  def should_generate_new_friendly_id?
    name_changed?
  end

  validates :name,  presence: true, uniqueness: { case_sensitive: false }

  mount_uploader :picture, AttachmentUploader
  validates :picture, :presence => true
end
