class User < ActiveRecord::Base
  attr_accessor :remember_token
  before_save   :downcase_email
  has_many :votes, dependent: :destroy

  before_save { self.email = email.downcase }
  validates :name,  presence: true, length: { maximum: 50, message: "name is too long" }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :gender, inclusion: { in: %w(m f), message: "%{value} is not m or f" }
  validates :affiliation, inclusion: { in: %w(Democrat Republican Independent Unaffiliated), message: "%{value} is not Democrat, Republican, Independent, or Unaffiliated" }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  class << self
    # Returns the hash digest of the given string.
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    def total_users
      User.all.count
    end

    def male_users
      User.where(:gender => 'm').count
    end

    def female_users
      User.where(:gender => 'f').count
    end

    def democrat_users
      User.where(:affiliation => 'Democrat').count
    end

    def republican_users
      User.where(:affiliation => 'Republican').count
    end

    def independent_users
      User.where(:affiliation => 'Independent').count
    end

    def unaffiliated_users
      User.where(:affiliation => 'Unaffiliated').count
    end

    def under_twenties_users
      User.where(:age => 0..19).count
    end

    def twenties_users
      User.where(:age => 20..29).count
    end

    def thirties_users
      User.where(:age => 30..39).count
    end

    def fourties_users
      User.where(:age => 40..49).count
    end

    def fifty_up_users
      User.where(:age => 50..150).count
    end

    def percent_of_type(user_type)
      (User.send("#{user_type}_users").to_f / User.all.count.to_f)*100
    end

    # Returns a random token.
    def new_token
      SecureRandom.urlsafe_base64
    end

  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  #def activate
  #  update_attribute(:activated,    true)
  #  update_attribute(:activated_at, Time.zone.now)
  #end

  # Sends activation email.
  #def send_activation_email
  #  UserMailer.account_activation(self).deliver_now
  #end

  # Sets the password reset attributes.
  #def create_reset_digest
  #  self.reset_token = User.new_token
  #  update_attribute(:reset_digest,  User.digest(reset_token))
  #  update_attribute(:reset_sent_at, Time.zone.now)
  #end

  # Returns true if a password reset has expired.
  #def password_reset_expired?
  #  reset_sent_at < 2.hours.ago
  #end

  # Sends password reset email.
  #def send_password_reset_email
  #  UserMailer.password_reset(self).deliver_now
  #end

  private

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    #def create_activation_digest
    #  self.activation_token  = User.new_token
    #  self.activation_digest = User.digest(activation_token)
    #end

    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    #def create_activation_digest
    #  self.activation_token  = User.new_token
    #  self.activation_digest = User.digest(activation_token)
    #end

end
