class BiasesController < ApplicationController
  before_action :admin_user

  def index
    @biases = Bias.all;
  end

  def show
    @bias = Bias.find(params[:id])
  end

  def new
    @bias = Bias.new
  end

  def create
    @bias = Bias.new(bias_params)
    if @bias.save
      flash[:success] = "Bias Added!"
      redirect_to @bias
    else
      render 'new'
    end
  end

  def edit
    @bias = Bias.find(params[:id])
  end

  def update
    @bias = Bias.find(params[:id])
    if @bias.update_attributes(bias_params)
      flash[:success] = "Bias updated"
      redirect_to @bias
    else
      render 'edit'
    end
  end

  def destroy
    @bias = Bias.find(params[:id])
    flash[:success] = "Bias deleted"
    redirect_to biases_index_path
  end

  private

    def bias_params
      params.require(:bias).permit(:name, :picture, :option1, :option3, :category_id)
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user && current_user.admin?
    end
end
