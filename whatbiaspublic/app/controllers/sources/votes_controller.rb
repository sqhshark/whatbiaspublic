class Sources::VotesController < VotesController
  before_action :set_voteable

  private

    def set_voteable
      @voteable = Source.friendly.find(params[:source_id])
    end
end
