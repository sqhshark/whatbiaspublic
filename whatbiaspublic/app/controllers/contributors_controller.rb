class ContributorsController < ApplicationController
  before_action :admin_user,     only: [:destroy, :edit, :approve]

  def index
    @contributors = Contributor.where(:approved => true).paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @contributor = Contributor.friendly.find(params[:id])
    if(!@contributor.approved?)
      redirect_to contributors_path
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to contributors_path
  end

  def new
    @contributor = Contributor.new
  end

  def create
    @contributor = Contributor.new(contributor_params)
    if @contributor.save
      if params[:category_ids]
        params[:category_ids].each do |categories|
          categories = ContributorCategory.new(:category_id => categories, :contributor_id => @contributor.id)
          if categories.valid?
            categories.save
          else
            render 'new'
          end
        end
      end

      flash[:success] = "Contributor added, awaiting admin approval."
      redirect_to @contributor
    else
      render 'new'
    end
  end

  def edit
    @contributor = Contributor.friendly.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to contributors_path
  end

  def update
    @contributor = Contributor.friendly.find(params[:id])
    if @contributor.update_attributes(contributor_params)
      flash[:success] = "Contributor updated"
      redirect_to @contributor
    else
      render 'edit'
    end
  end

  def destroy
    Contributor.friendly.find(params[:id]).destroy
    flash[:success] = "Contributor deleted"
    redirect_to contributors_approve_path
  end

  def approve
    @contributors = Contributor.where(:approved => false).paginate(:page => params[:page])
  end

  def approved
    @contributor = Contributor.friendly.find(params[:id])
    if current_user && current_user.admin?
      @contributor.update(approved: true)
      flash[:success] = "Contributor approved"
      redirect_to @contributor
    else
      render 'approve'
    end
  end

  private

    def approved_params
      params.require(:contributor).permit(:approved)
    end

    def contributor_params
      params.require(:contributor).permit(:name, :picture, :source_id, category_ids:[])
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user && current_user.admin?
    end
end
