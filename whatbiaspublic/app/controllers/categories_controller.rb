class CategoriesController < ApplicationController
  def show
    begin
      @sources = Category.friendly.find(params[:id]).sources
      @contributors = Category.friendly.find(params[:id]).contributors
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path
    end

  end
end
