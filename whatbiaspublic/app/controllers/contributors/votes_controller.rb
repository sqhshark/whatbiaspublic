class Contributors::VotesController < VotesController
  before_action :set_voteable

  private

    def set_voteable
      @voteable = Contributor.friendly.find(params[:contributor_id])
    end
end
