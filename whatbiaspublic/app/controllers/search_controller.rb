class SearchController < ApplicationController

  def search
    @sources = Source.where('name ilike ?', "%#{params[:search]}%").paginate(:page => params[:page], :per_page => 10)
    @contributors = Contributor.where('name ilike ?', "%#{params[:search]}%").paginate(:page => params[:page], :per_page => 10)
  end

end
