 class VotesController < ApplicationController
   before_action :valid_user

   def create
     if user_has_voted?(params[:vote][:bias_id])
       @vote = @voteable.votes.find_by(bias_id: params[:vote][:bias_id], user_id: current_user.id)
       if @vote.update_attributes(vote_params)
         flash[:success] = "Vote updated"
         redirect_to @voteable
       else
         redirect_to @voteable
       end
     else
       @vote = @voteable.votes.new vote_params
       @vote.user = current_user
       @vote.save
       flash[:success] = "Your vote was successfully cast"
       redirect_to @voteable
     end
   end

   private

     # Confirms a valid user.
     def valid_user
       unless (current_user)
         flash[:warning] = "You must be logged in to vote."
         redirect_to root_url
       end
     end

     def user_has_voted?(bias_id)
       @voteable.votes.where(bias_id: bias_id, user_id: current_user.id).count >= 1
     end

    def vote_params
      params.require(:vote).permit(:bias_id, :option)
    end
 end
