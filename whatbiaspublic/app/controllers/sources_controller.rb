class SourcesController < ApplicationController

  before_action :admin_user,     only: [:destroy, :edit, :approve]

  def index
    @sources = Source.where(:approved => true).paginate(:page => params[:page], :per_page => 10)
  end

  def show
    @source = Source.friendly.find(params[:id])
    @contributors = @source.contributors.all
    if(!@source.approved?)
      redirect_to sources_path
    end
    rescue ActiveRecord::RecordNotFound
      redirect_to sources_path
  end

  def new
    @source = Source.new
  end

  def create
    @source = Source.new(source_params)
    if @source.save
      if params[:category_ids]
        params[:category_ids].each do |categories|
          categories = SourceCategory.new(:category_id => categories, :source_id => @source.id)
          if categories.valid?
            categories.save
          else
            render 'new'
          end
        end
      end

      flash[:success] = "Source added, awaiting admin approval."
      redirect_to @source
    else
      render 'new'
    end
  end

  def edit
    @source = Source.friendly.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to sources_path
  end

  def update
    @source = Source.friendly.find(params[:id])
    if @source.update_attributes(source_params)
      flash[:success] = "Source updated"
      redirect_to @source
    else
      render 'edit'
    end
  end

  def destroy
    Source.friendly.find(params[:id]).destroy
    flash[:success] = "Source deleted"
    redirect_to sources_approve_path
  end

  def approve
    @sources = Source.where(:approved => false).paginate(:page => params[:page])
  end

  def approved
    @source = Source.friendly.find(params[:id])
    if current_user && current_user.admin?
      @source.update(approved: true)
      flash[:success] = "Source approved"
      redirect_to @source
    else
      render 'approve'
    end
  end

  def vote
    @source = Source.friendly.find(params[:id])
    if @source && current_user
      @source.build_vote(user_id: current_user.id, bias_id: params[:bias_id], option: 0)
      redirect_to :back, notice: "Thank you for voting."
    else
      redirect_to :back, alert: "Unable to vote, not logged in."
    end
  end


  private

    def approved_params
      params.require(:source).permit(:approved)
    end

    def source_params
      params.require(:source).permit(:name, :logo, category_ids:[])
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user && current_user.admin?
    end

end
