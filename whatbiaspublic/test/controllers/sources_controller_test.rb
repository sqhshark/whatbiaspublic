require 'test_helper'

class SourcesControllerTest < ActionController::TestCase

  def setup
    @user = users(:michael)
    @non_admin = users(:archer)
    @source = Source.create!(name: "FakeSource",
                   approved: 1,
                   id: 1,
                   remote_logo_url: "http://i.cdn.turner.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png")
  end

  #index
  test "should get index" do
    get :index
    assert_response :success
  end

  #show
  test "should get show" do
    get :show, id: @source
    assert_response :success
  end

  test "should redirect when show not found" do
    get :show, id: 9999999
    assert_redirected_to sources_path
  end

  #new
  test "should get new" do
    get :new
    assert_response :success
  end

  #edit
  test "should get edit" do
    log_in_as(@user)
    get :edit, id: @source
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @source
    assert_redirected_to root_url
  end

  test "should redirect edit when not admin" do
    log_in_as(@non_admin)
    get :edit, id: @source
    assert_redirected_to root_url
  end

  test "should redirect when edit not found" do
    log_in_as(@user)
    get :edit, id: 9999999
    assert_redirected_to sources_path
  end

  #approve
  test "should get approve" do
    log_in_as(@user)
    get :approve
    assert_response :success
  end

  test "should redirect approve when not logged in" do
    get :approve
    assert_redirected_to root_url
  end

  test "should redirect approve when not admin" do
    log_in_as(@non_admin)
    get :approve
    assert_redirected_to root_url
  end
end
