require 'test_helper'

class SourceTest < ActiveSupport::TestCase
  def setup
    @source = Source.new(name: "FakeSource",
                   approved: 1,
                   id: 1,
                   remote_logo_url: "http://i.cdn.turner.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png")
  end

  test "source should be valid" do
    assert @source.valid?
  end

  test "source name should be present" do
    @source.name = "     "
    assert_not @source.valid?
  end

  test "source logo should be present" do
    @source = Source.new(name: "aacaac", approved: 1, id:2)
    assert_not @source.valid?
  end

  test "source name should be unique" do
    duplicate_source = @source.dup
    duplicate_source.name = @source.name.upcase
    @source.save
    assert_not duplicate_source.valid?
  end

end
