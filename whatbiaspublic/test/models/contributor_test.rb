require 'test_helper'

class ContributorTest < ActiveSupport::TestCase
  def setup
    @contributor = Contributor.new(name: "FakeSource",
                   approved: 1,
                   id: 1,
                   remote_picture_url: "http://i.cdn.turner.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png")
  end

  test "contributor should be valid" do
    assert @contributor.valid?
  end

  test "contributor name should be present" do
    @contributor.name = "     "
    assert_not @contributor.valid?
  end

  test "contributor picture should be present" do
    @contributor = Contributor.new(name: "aacaac", approved: 1, id:2)
    assert_not @contributor.valid?
  end

  test "contributor name should be unique" do
    duplicate_source = @contributor.dup
    duplicate_source.name = @contributor.name.upcase
    @contributor.save
    assert_not duplicate_source.valid?
  end
end
