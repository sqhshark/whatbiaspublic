require 'test_helper'

class BiasTest < ActiveSupport::TestCase

  #start by setting up a valid object
  def setup
    @bias = Bias.new(name: "Random Name", option1: "o1", option3:"o3",
                     remote_picture_url: "http://i.cdn.turner.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png")
  end

  test "bias be valid" do
    assert @bias.valid?
  end

  test "bias name should be present" do
    @bias.name = "     "
    assert_not @bias.valid?
  end

  test "bias option1 should be present" do
    @bias.option1 = "     "
    assert_not @bias.valid?
  end

  test "bias option3 should be present" do
    @bias.option3 = "     "
    assert_not @bias.valid?
  end

  test "bias picture should be present" do
    @bias2 = Bias.new(name: "Random Name", option1: "o1", option3:"o3", id:1)
    assert_not @bias2.valid?
  end


end
