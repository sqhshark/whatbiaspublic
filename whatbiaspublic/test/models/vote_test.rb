require 'test_helper'

class VoteTest < ActiveSupport::TestCase
  #start by setting up a valid object
  def setup
    @vote = Vote.new(option: 1)
  end

  test "vote be valid" do
    assert @vote.valid?
  end

  test "vote option should be present" do
    @vote.option = nil
    assert_not @vote.valid?
  end
end
