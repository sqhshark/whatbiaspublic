require 'test_helper'

class SourceCreateTest < ActionDispatch::IntegrationTest

  def setup
    @source = Source.create!(name: "FakeSource",
                   approved: 1,
                   id: 1,
                   remote_logo_url: "http://i.cdn.turner.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png")

    @user = users(:michael)
  end

  test "invalid source creation" do
    get new_source_path
    assert_no_difference 'Source.count' do
      post sources_path, source: { name:  "",
                                   logo: "logo" }
    end
    assert_template 'sources/new'
    #assert_select 'div#<CSS id for error explanation>'
    #assert_select 'div.<CSS class for field with error>'
  end

  test "valid source creation" do
    get new_source_path
    assert_template 'sources/new'
    #cat_ids = [0,1]

    #assert_difference 'Source.count', 1 do
    #  post_via_redirect sources_path, source: { name:  "kll",
    #                                            logo: @source.logo,
    #                                            category_ids: cat_ids }
    #end
  end

end
