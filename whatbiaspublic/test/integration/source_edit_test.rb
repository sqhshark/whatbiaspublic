require 'test_helper'

class SourceEditTest < ActionDispatch::IntegrationTest

  def setup
    @source = Source.create!(name: "FakeSource",
                   approved: 1,
                   id: 1,
                   remote_logo_url: "http://i.cdn.turner.com/cnn/.e1mo/img/4.0/logos/logo_cnn_badge_2up.png")

    @user = users(:michael)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_source_path(@source)
    assert_template 'sources/edit'
    patch source_path(@source), source: { name:  "",
                                          logo: "",
                                        }
    assert_template 'sources/edit'
  end

  test "successful edit" do
    log_in_as(@user)
    get edit_source_path(@source)
    assert_template 'sources/edit'
    name  = "jknkjnknkjnkjn"
    cat_ids = [0,1]
    patch source_path(@source), source: { name: name, remote_logo_url: "https://pbs.twimg.com/profile_images/378800000173019318/8db7be984cfb21b5c6701dd9b6a1bb62_400x400.jpeg", category_ids: cat_ids }
    assert_not flash.empty?
    @source.reload
    assert_redirected_to @source
    assert_equal name,  @source.name
    #assert_equal logo,  @source.logo
  end

end
