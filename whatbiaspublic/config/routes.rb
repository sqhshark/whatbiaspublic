Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  root             'sources#index'
  get 'users'    => 'static_pages#users'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  get    'sources/approve'   => 'sources#approve'
  patch    'sources/approved'   => 'sources#approved'
  post    'sources/approved'   => 'sources#approved'
  get    'contributors/approve'   => 'contributors#approve'
  patch    'contributors/approved'   => 'contributors#approved'
  post    'contributors/approved'   => 'contributors#approved'

  get    'search/search'   => 'search#search'

  resources :users
  resources :biases
  resources :sources do
    resources :votes, module: :sources
  end
  resources :contributors do
    resources :votes, module: :contributors
  end
  #resources :account_activations, only: [:edit]
  #resources :password_resets,     only: [:new, :create, :edit, :update]

  resources :categories, only: [:show]
end
